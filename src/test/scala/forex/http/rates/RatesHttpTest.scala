package forex.http
package rates

import java.time.ZoneOffset

import cats.effect.{IO, Resource}
import forex.clients.OneFrameClient
import forex.config.OneFrameConfig
import forex.domain.Currency
import forex.domain.Currency.{SGD, USD, show}
import forex.domain.Rate.Pair
import forex.http.rates.Protocol.GetApiResponse
import forex.programs.RatesProgram
import forex.services.{RatesService, RatesServices}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder, Json}
import org.http4s._
import org.http4s.circe.jsonEncoderOf
import org.http4s.client.Client
import org.http4s.client.dsl.Http4sClientDsl
import org.http4s.dsl.Http4sDsl
import org.http4s.implicits.{http4sKleisliResponseSyntaxOptionT, http4sLiteralsSyntax}
import org.scalacheck.{Arbitrary, Gen}
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers.convertToAnyShouldWrapper
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks
import collection.JavaConverters._

class RatesHttpTest extends AnyFunSuite with ScalaCheckPropertyChecks with Http4sDsl[IO] with Http4sClientDsl[IO] {

  implicit val rateEncoder: Encoder[OneFrameClient.Rate]       = deriveEncoder
  implicit val rateEnc: EntityEncoder[IO, OneFrameClient.Rate] = jsonEncoderOf
  implicit val currencyEncoder: Encoder[Currency] =
    Encoder.instance[Currency] { show.show _ andThen Json.fromString }

  implicit val apiRespDecoder: Decoder[GetApiResponse] = deriveDecoder

  def rate(pair: Pair): Gen[OneFrameClient.Rate] = Arbitrary[OneFrameClient.Rate] {
    for {
      ask <- Gen.posNum[Long]
      bid <- Gen.posNum[Long]
      price <- Gen.posNum[Long]
      time_stamp <- Gen.calendar
    } yield OneFrameClient.Rate(ask, bid, pair.from, price, time_stamp.toInstant.atOffset(ZoneOffset.UTC), pair.to)
  }.arbitrary

  // generate rates of currency pairs that are in my application
  val fixedRates = Gen.sequence(Pair.allPairs.map(rate(_)))

  test("get rates should the rate from the one-frame client") {
    forAll(fixedRates) { rates =>
      val client = Client[IO](_ => {
        Resource.liftF(Ok(rates.asScala.toList))
      })

      val ratesService: RatesService[IO] =
        RatesServices.oneFrame(OneFrameClient.impl(OneFrameConfig("doesn't matter", 8888, "cafebabe"), client))

      val ratesProgram: RatesProgram[IO] = RatesProgram[IO](ratesService)

      val ratesHttpRoutes: HttpRoutes[IO] = new RatesHttpRoutes[IO](ratesProgram).routes

      val getUsdToSgd: Request[IO] = Request[IO](Method.GET, uri"/rates?from=SGD&to=USD")

      val resp: Response[IO] = ratesHttpRoutes.orNotFound(getUsdToSgd).unsafeRunSync()
      resp.status shouldEqual Ok
      val maybeRate = rates.asScala.find(p => p.from == SGD && p.to == USD)
      val actualPrice = resp.as[GetApiResponse].unsafeRunSync().price.value.bigDecimal
      val expectedPrice = maybeRate.get.price.bigDecimal
      actualPrice shouldEqual expectedPrice
    }
  }
}
