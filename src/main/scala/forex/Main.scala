package forex

import cats.effect.IO.ioConcurrentEffect
import cats.effect._
import forex.clients.CachingMiddleware
import forex.config._
import fs2.Stream
import org.http4s.client.blaze.BlazeClientBuilder
import org.http4s.client.dsl.Http4sClientDsl
import org.http4s.server.blaze.BlazeServerBuilder

import scala.concurrent.ExecutionContext.global

object Main extends IOApp with Http4sClientDsl[IO] {
  override def run(args: List[String]): IO[ExitCode] = {
    new Application[IO].stream.compile.drain.as(ExitCode.Success)
  }
}

class Application[F[_] : ConcurrentEffect : Timer] {

  def stream: Stream[F, Unit] =
    for {
      config <- Config.stream("app")
      client <- BlazeClientBuilder(global).stream
      cachingMiddleware <- CachingMiddleware.stream()
      cachedClient = cachingMiddleware(client)
      module = new Module[F](config, cachedClient)
      _ <- BlazeServerBuilder[F](global)
        .bindHttp(config.http.port, config.http.host)
        .withHttpApp(module.httpApp)
        .serve
    } yield ()

}
