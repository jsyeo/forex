package forex.domain

case class Rate(
    pair: Rate.Pair,
    price: Price,
    timestamp: Timestamp
)

object Rate {
  final case class Pair(
      from: Currency,
      to: Currency
  )

  object Pair {
    val allPairs: Seq[Pair] = Currency.currencies.combinations(2).map(l => Pair(l.head, l.tail.head)).toStream

    def flip(pair: Pair) = Pair(from = pair.to, to = pair.from)
  }

  def inverse(rate: Rate) = Rate(Pair(from = rate.pair.to, to = rate.pair.from), Price(1 / rate.price.value), rate.timestamp)
}
