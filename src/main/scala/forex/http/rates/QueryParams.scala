package forex.http.rates

import cats.data.NonEmptyList
import cats.implicits._
import forex.domain.Currency
import org.http4s.dsl.impl.ValidatingQueryParamDecoderMatcher
import org.http4s.{ParseFailure, QueryParamDecoder, QueryParameterValue}

object QueryParams {

  private[http] implicit val currencyQueryParam: QueryParamDecoder[Currency] = (value: QueryParameterValue) => {
    Either.catchOnly[MatchError](Currency.fromString(value.value))
      .leftMap(t => NonEmptyList.one(ParseFailure(t.getMessage, t.getMessage)))
      .toValidated
  }

  object FromQueryParam extends ValidatingQueryParamDecoderMatcher[Currency]("from")
  object ToQueryParam extends ValidatingQueryParamDecoderMatcher[Currency]("to")

}
