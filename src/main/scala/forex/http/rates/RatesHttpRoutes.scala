package forex.http
package rates

import cats.effect.Sync
import cats.syntax.flatMap._
import forex.programs.RatesProgram
import forex.programs.rates.errors.Error.{ApiConnectionError, RateLookupFailed}
import forex.programs.rates.{Protocol => RatesProgramProtocol}
import org.http4s.HttpRoutes
import org.http4s.dsl.Http4sDsl
import org.http4s.server.Router

class RatesHttpRoutes[F[_] : Sync](rates: RatesProgram[F]) extends Http4sDsl[F] {

  import Converters._
  import Protocol._
  import QueryParams._

  private[http] val prefixPath = "/rates"

  private val httpRoutes: HttpRoutes[F] = HttpRoutes.of[F] {
    case GET -> Root :? FromQueryParam(fromValidated) +& ToQueryParam(toValidated) =>
      // 422 is more appropriate since 400 (bad request) is for bad http syntax
      // See https://tools.ietf.org/html/rfc4918
      fromValidated.fold(_ => UnprocessableEntity("Invalid from currency"), from =>
        toValidated.fold(_ => UnprocessableEntity("Invalid to currency"), to =>
          if (from == to)
            UnprocessableEntity("Both input currencies are the same")
          else
            rates.get(RatesProgramProtocol.GetRatesRequest(from, to)).flatMap {
              case Left(ApiConnectionError(msg)) => ServiceUnavailable(msg)
              case Left(RateLookupFailed(msg)) => InternalServerError(msg)
              case Right(rate) => Ok(rate.asGetApiResponse)
            }
        )
      )
  }

  val routes: HttpRoutes[F] = Router(
    prefixPath -> httpRoutes
  )
}
