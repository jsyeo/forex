package forex.clients

import java.time.OffsetDateTime

import cats.Applicative
import cats.data.EitherT
import cats.effect.Sync
import cats.implicits.catsSyntaxApplicativeError
import forex.config.OneFrameConfig
import forex.domain.{Currency, Rate}
import forex.services.rates.errors.Error
import forex.services.rates.errors.Error._
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}
import org.http4s.Method.GET
import org.http4s.Uri.{Authority, RegName}
import org.http4s._
import org.http4s.circe.CirceEntityCodec.circeEntityDecoder
import org.http4s.circe.{jsonEncoderOf, jsonOf}
import org.http4s.client.Client

/**
 * A client that handles connection to the one-frame api.
 */
trait OneFrameClient[F[_]] {
  def rates(pairs: Seq[Rate.Pair]): F[Error Either List[OneFrameClient.Rate]]
}

object OneFrameClient {

  final case class Rate(ask: BigDecimal,
                        bid: BigDecimal,
                        from: Currency,
                        price: BigDecimal,
                        time_stamp: OffsetDateTime,
                        to: Currency)

  object Rate {
    implicit val rateDecoder: Decoder[Rate] = deriveDecoder[Rate]

    implicit def rateEntityDecoder[F[_] : Sync]: EntityDecoder[F, Rate] = jsonOf

    implicit val rateEncoder: Encoder[Rate] = deriveEncoder[Rate]

    implicit def rateEntityEncoder[F[_] : Applicative]: EntityEncoder[F, Rate] = jsonEncoderOf
  }

  /**
   * A higher order function that returns an implementation of the [[OneFrameClient]].
   *
   * @param config The configurations required to connect to the one-frame api.
   * @param client The HTTP client
   * @tparam F
   * @return an implementation of the one-frame client
   */
  def impl[F[_] : Sync](config: OneFrameConfig, client: Client[F]): OneFrameClient[F] = {
    val baseUri = Uri(authority = Some(Authority(host = RegName(config.host), port = Some(config.port))))
    val headers = Headers.of(Header("token", config.token))
    pairs => {
      val pairArgs = pairs.foldLeft(List[String]()) { (l: List[String], p: forex.domain.Rate.Pair) =>
        s"${p.from}${p.to}" :: l
      }
      val uri = baseUri
        .addPath("/rates")
        .withQueryParam("pair", pairArgs)

      val req = Request[F](GET, uri, headers = headers)
      EitherT(client.expect[List[Rate]](req).attempt).fold(
        _ => Left(OneFrameConnectionError("Error connecting to one-frame api")),
        {
          case rs@_ if rs.size == pairs.size => Right(rs) // Either.Right only when the input pairs.size == output rates.size
          case Nil => Left(OneFrameLookupFailed("No rates found"))
          case _ => Left(OneFrameLookupFailed(s"one-frame API returned fewer rates than requested, $pairs"))
        })
    }
  }
}
