package forex.clients

import cats.effect.Sync
import cats.implicits.catsSyntaxOptionId
import fs2.Stream
import io.chrisdavenport.cats.effect.time.JavaTime
import io.chrisdavenport.mules.TimeSpec
import io.chrisdavenport.mules.caffeine.CaffeineCache
import io.chrisdavenport.mules.http4s.{CacheItem, CacheMiddleware, CacheType}
import org.http4s.client.Client
import org.http4s.{Method, Uri}

import scala.concurrent.duration.{DurationInt, FiniteDuration, MINUTES}

/**
 * A caching middleware that wraps around [[org.http4s.client.Client]]s so that we can
 * work around the one-frame api's limit of 1000 requests a day.
 */
object CachingMiddleware {

  private final val expireAfter = FiniteDuration((5 minutes).toMinutes, MINUTES)

  private final val cacheSize = (2L ^ 14).some

  /**
   * In pure FP style, a client middleware is just a function that takes a client and produces a new client.
   */
  type ClientMiddleware[F[_]] = Client[F] => Client[F]

  def stream[F[_]: Sync: JavaTime](): Stream[F, ClientMiddleware[F]] = {
      Stream.eval(CaffeineCache.build[F, (Method, Uri), CacheItem](TimeSpec.fromDuration(expireAfter), None, cacheSize)).map(cache =>
        CacheMiddleware.client(cache, CacheType.Public)
      )
  }
}
