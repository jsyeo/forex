package forex.services.rates.interpreters

import cats.data.EitherT
import cats.effect.Sync
import cats.implicits._
import forex.clients.OneFrameClient
import forex.domain.{Price, Rate, Timestamp}
import forex.services.rates.{Algebra, errors}

class OneFrame[F[_] : Sync](val client: OneFrameClient[F]) extends Algebra[F] {
  override def get(pair: Rate.Pair): F[Either[errors.Error, Rate]] = getRates(Rate.Pair.allPairs).map {
    case Left(e) => e.asLeft[Rate]
    case Right(rates) =>
      // find the input pair or find the inverse of the pair
      val maybeRate = rates.find(r => r.pair == pair || Rate.Pair.flip(r.pair) == pair)
      maybeRate match {
        case Some(rate) =>
          val r = if (rate.pair == pair)
            rate
          else
            // we cached the inverse of the exchange rate, let's inverse the rate
            Rate.inverse(rate)
          r.asRight[errors.Error]
        case None => errors.Error.OneFrameUnknownPair(s"Couldn't find $pair in all combinations of known currency pair").asLeft[Rate]
      }
  }

  private def getRates(pairs: Seq[Rate.Pair]): F[Either[errors.Error, List[Rate]]] =
    EitherT(client.rates(pairs)).map(_.map(r => {
      Rate(Rate.Pair(r.from, r.to), Price(r.price), Timestamp(r.time_stamp))
    })).value
}
