package forex.services.rates

import cats.effect.Sync
import forex.clients.OneFrameClient
import forex.services.rates.interpreters._

object Interpreters {
  def oneFrame[F[_]: Sync](client: OneFrameClient[F]): Algebra[F] = new OneFrame[F](client)
}
